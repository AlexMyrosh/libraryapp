﻿using System;
using System.Collections.Generic;

namespace LibraryDAL.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }

        public int YearOfPublishing { get; set; }

        public int Amount { get; set; }

        public Genre Genre { get; set; }

        public IEnumerable<AuthorBook> Authors { get; set; }
    }
}
