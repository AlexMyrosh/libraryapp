﻿using System;
using System.Collections.Generic;

namespace LibraryDAL.Entities
{
    public class Author : BaseEntity
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public IEnumerable<AuthorBook> Books { get; set; }
    }
}
