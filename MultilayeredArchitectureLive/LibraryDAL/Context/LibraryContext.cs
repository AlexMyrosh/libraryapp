﻿using LibraryDAL.Entities;
using System;
using System.Collections.Generic;

namespace LibraryDAL.Context
{
    public class LibraryContext
    {
        public IEnumerable<Author> Authors { get; set; }

        public IEnumerable<Book> Books { get; set; }

        public void Initialize()
        {
            // authors 

            Author firstAuthor = new Author()
            {
                FirstName = "Taras",
                LastName = "Shevchenko",
                DateOfBirth = DateTime.UtcNow,
            };
            Author secondAuthor = new Author()
            {
                FirstName = "Alexandr",
                LastName = "Pushkin",
                DateOfBirth = DateTime.UtcNow
            };
            Author thirdAuthor = new Author()
            {
                FirstName = "Serhii",
                LastName = "Zhadan",
                DateOfBirth = DateTime.UtcNow
            };

            // books

            Book firstBook = new Book()
            {
                Title = "Mesopotamia",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 100,
                Genre = Genre.Fiction,
                Authors = new List<AuthorBook>() { new AuthorBook() { AuthorId = thirdAuthor.Id } }
            };
            Book secondBook = new Book()
            {
                Title = "Kateryna",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 1000,
                Genre = Genre.Novel,
                Authors = new List<AuthorBook>() { new AuthorBook() { AuthorId = firstAuthor.Id } }
            };
            Book thirdBook = new Book()
            {
                Title = "Poems",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 1000,
                Genre = Genre.Pomance,
                Authors = new List<AuthorBook>() { new AuthorBook() { AuthorId = secondAuthor.Id } }
            };
            Book fourthBook = new Book()
            {
                Title = "The New York Times",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 10000,
                Genre = Genre.Periodic
            };
            Book fifthBook = new Book()
            {
                Title = "Fifth book",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 10000,
                Genre = Genre.Adventures,
                Authors = new List<AuthorBook>()
                {
                    new AuthorBook() { AuthorId=firstAuthor.Id },
                    new AuthorBook() { AuthorId=secondAuthor.Id },
                    new AuthorBook() { AuthorId=thirdAuthor.Id },
                }
            };

            Authors = new List<Author>()
            {
                firstAuthor,
                secondAuthor,
                thirdAuthor
            };

            Books = new List<Book>()
            {
                firstBook,
                secondBook,
                thirdBook,
                fourthBook,
                fifthBook
            };

        }
    }
}
